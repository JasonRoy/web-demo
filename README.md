# CI/CD Pipline Demo

**master**: <br/>
[![pipeline status](https://gitlab.com/JasonRoy/web-demo/badges/master/pipeline.svg)](https://gitlab.com/JasonRoy/web-demo/-/commits/master)
[![coverage report](https://gitlab.com/JasonRoy/web-demo/badges/master/coverage.svg)](https://gitlab.com/JasonRoy/web-demo/-/commits/master)

**develop**<br/>
[![pipeline status](https://gitlab.com/JasonRoy/web-demo/badges/develop/pipeline.svg)](https://gitlab.com/JasonRoy/web-demo/-/commits/develop) [![coverage report](https://gitlab.com/JasonRoy/web-demo/badges/develop/coverage.svg)](https://gitlab.com/JasonRoy/web-demo/-/commits/develop)

A demonstration of the powerful Gitlab CI created by Jason Roy and Benjamin Kenner.<br/>
Used a [Full Stack Web App](https://milanwittpohl.com/projects/tutorials/Full-Stack-Web-App/) as Example Project. The Project was created by Milan Wittpohl.


