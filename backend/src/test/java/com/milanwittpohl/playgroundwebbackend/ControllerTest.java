package com.milanwittpohl.playgroundwebbackend;

import com.milanwittpohl.playgroundwebbackend.controller.ToDoController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ControllerTest {

    @Autowired
    private ToDoController toDoController;


    @Test
    public void contextLoads() throws Exception {
        assertThat(toDoController).isNotNull();
    }

}
